# Register well-known SPA factories
add-spa-lib audio.convert* audioconvert/libspa-audioconvert
add-spa-lib api.alsa.* alsa/libspa-alsa
add-spa-lib api.v4l2.* v4l2/libspa-v4l2
add-spa-lib api.bluez5.* bluez5/libspa-bluez5
add-spa-lib api.vulkan.* vulkan/libspa-vulkan
add-spa-lib api.jack.* jack/libspa-jack

# Create the sessions
create-session audio
create-session video

# Load session item factories (building blocks for the session management graph)
# Do not comment these out unless you really know what you are doing
load-module C libwireplumber-module-si-standard-link
load-module C libwireplumber-module-si-adapter
load-module C libwireplumber-module-si-convert
load-module C libwireplumber-module-si-fake-stream
load-module C libwireplumber-module-si-simple-node-endpoint
load-module C libwireplumber-module-si-audio-softdsp-endpoint
load-module C libwireplumber-module-si-monitor-endpoint
load-module C libwireplumber-module-si-bluez5-endpoint

# Additional PipeWire modules can also be loaded in WirePlumber like this.
# libpipewire already loads all the modules that we normally need, though.
# spa-node-factory may be needed if you want to use a monitor with 'local-nodes'
# but without 'use-adapter' (adapter is loaded by default, but spa-node-factory isn't)
#load-pipewire-module libpipewire-module-spa-node-factory


#
# Load device monitors
#
# factory: the SPA factory name to load (must be registered above)
# flags:
#   use-adapter: Use 'adapter' instead of 'spa-node-factory' to create the nodes
#     (you normally want this for audio nodes)
#   local-nodes: Run the nodes in the WirePlumber process instead of the
#     PipeWire one (useful for high latency devices, to avoid stalling PipeWire)
#

load-module C libwireplumber-module-monitor {
  "alsa":   <{"factory": <"api.alsa.enum.udev">,   "flags": <["use-acp", "use-adapter"]>}>,
  "bluez5": <{"factory": <"api.bluez5.enum.dbus">, "flags": <["local-nodes", "use-adapter"]>}>,
  "v4l2":   <{"factory": <"api.v4l2.enum.udev">}>
}

#
# Load functionality plugins
#

# Grants dbus reservation functionality
load-module C libwireplumber-module-dbus-reservation

# Grants functionality to store and restaure default device profiles
load-module C libwireplumber-module-default-profile

# Grants access to security confined clients
load-module C libwireplumber-module-client-permissions

# Automatically sets device profiles to 'On'
load-module C libwireplumber-module-device-activation

# Automatically suspends idle nodes after 3 seconds
load-module C libwireplumber-module-node-suspension

# Sets endpoint defaults on each session and stores default endpoint
# user settings in XDG_CONFIG_DIR
load-module C libwireplumber-module-session-settings

# Implements static objects creation based on TOML configuration files
load-module C libwireplumber-module-config-static-objects

# Implements creation of endpoints
load-module C libwireplumber-module-endpoint-creation

# Implements linking clients to devices based on TOML configuration files
load-module C libwireplumber-module-config-policy

# Implements storing metadata about objects in RAM
load-module C libwireplumber-module-metadata
